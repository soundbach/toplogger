"""
Copyright 2020 Bach
"""
import requests
import pandas as pd
import datetime as dt
import sys
import time
import smtplib

if len(sys.argv) != 3:
    sys.exit('required arguments not passed:\npass preferred starting time interval')

password = input('enter email password: ')

fromaddr='@gmail.com' ## gmail address used to send mails from
username='username' ## username for gmail address used to send mails from
recipients=[''] ## list of email addresses to send mails to

start_time = sys.argv[1] ## '20:00'
end_time = sys.argv[2] ## '21:30'

print(f'preferred starting time earliest: {start_time}')
print(f'preferred starting time latest:   {end_time}')

## only scraping today atm
date = dt.date.today().strftime('%Y-%m-%d') ## 2020-11-15, default is today

def scrape_and_find(start_time, end_time, date):
    """
    Scrapes Toplogger booking data, and checks if there are slots available at the preferred starttime interval.

    :param start_time: str. Format HH:MM. Preferred start of starting time interval
    :param end_time: str. Format HH:MM. Preferred end of starting time interval
    :param date: str. Format 'YYYY-MM-DD'. Date on which you want to boulder!
    
    :return: DataFrame: returns a DataFrame that contains the available spots in the preferred starting interval and date.
    """

    ## preferred timeslot start
    start_datetime = dt.datetime.strptime(date+'T'+start_time,'%Y-%m-%dT%H:%M')
    end_datetime = dt.datetime.strptime(date+'T'+end_time,'%Y-%m-%dT%H:%M')

    ## gyms: energiehaven 11 (reservation_area_id':'15'), zuidhaven 115, sterk 20 ('reservation_area_id':'4')
    gym = 20
    response = requests.get(f'https://api.toplogger.nu/v1/gyms/{gym}/slots',
                            params={'date':date,
                                    'reservation_area_id':'4',
                                    'slim':'true'})

    if response.status_code != 200:
        print(response.text)

        send_gmail(text=str(response.status_code), 
                   password=password, 
                   fromaddr=fromaddr, 
                   username=username, 
                   recipients=recipients)

        sys.exit(f'unexpected response. status code: {response.status_code}')

    data = pd.DataFrame(response.json())

    data['start_datetime'] = data.start_at.map(lambda x: dt.datetime.strptime(x[:16],'%Y-%m-%dT%H:%M'))
    data['start_date'] = data.start_datetime.map(lambda x: x.date())
    data['start_time'] = data.start_datetime.map(lambda x: x.time())

    return data[(data.start_datetime>=start_datetime) &
                (data.start_datetime<=end_datetime) &
                (data.spots_booked<data.spots)]

def scrape_multiple(start_time, end_time, date, wait_minutes=5):
    print(f'scraping: {dt.datetime.today()}')
    results = scrape_and_find(start_time=start_time, 
                              end_time=end_time, 
                              date=date)
    while len(results) == 0 and dt.datetime.today() <= dt.datetime.strptime(date+'T'+end_time,'%Y-%m-%dT%H:%M'):
        print(f'done, waiting {wait_minutes} minutes')
        time.sleep(60*wait_minutes)
        print(f'scraping: {dt.datetime.today()}')
        results = scrape_and_find(start_time=start_time, 
                                  end_time=end_time, 
                                  date=date)

    text = f'Subject: slots available for {date} at\n\n'
    for index, row in results.iterrows():
        text += f'{row.start_time} ({row.spots - row.spots_booked} slots)\n'
    text += 'click here to book: https://app.toplogger.nu/nl/sterk/bookings/book'
    print(text)

    return text

def send_gmail(password, text, fromaddr, username, recipients):
    """
    Sends mail from a gmail account. In gmail settings 'Allow less secure apps' should be enabled.
    """
    print('sending mail')
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(username, password)
    server.sendmail(fromaddr, recipients, text)
    server.quit()
    print('done, exiting')

text = scrape_multiple(start_time=start_time, 
                       end_time=end_time, 
                       date=date, 
                       wait_minutes=5)

send_gmail(text=text, 
           password=password, 
           fromaddr=fromaddr, 
           username=username, 
           recipients=recipients)